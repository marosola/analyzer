package com.agileengine.analyzer;

import jodd.io.FileUtil;
import jodd.jerry.Jerry;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


@SpringBootApplication
public class AnalyzerApplication {

    public static void main(String[] args) throws IOException {
        SpringApplication.run(AnalyzerApplication.class, args);

        if (args.length < 2) {
            System.out.println("Proper Usage is: java -cp <your_bundled_app>.jar <input_origin_file_path> <input_other_sample_file_path>");
            System.exit(0);
        }

        File fileOrigin = new File(args[0]);
        Jerry docOrigin = Jerry.jerry(FileUtil.readString(fileOrigin));
        Jerry button = docOrigin.$("#make-everything-ok-button").first();
        printFullLocation("Origin html file", button);

        File fileOther = new File(args[1]);
        Jerry docOther = Jerry.jerry(FileUtil.readString(fileOther));
        Jerry buttonOther = docOther.$("a:contains('Make everything OK')").first();
        printFullLocation("Other html file", buttonOther);
    }

    private static void printFullLocation(String title, Jerry rootElement) {
        if (rootElement.size() == 0) {
            System.out.println("Make everything OK button doesn't exist in this html");
            return;
        }

        List<String> locationNodes = new ArrayList<>();
        for (Jerry element = rootElement; element.get(0).getNodeName() != null; element = element.parent()) {
            if (element.get(0).getNodeName() != null) {
                locationNodes.add(element.get(0).getNodeName());
            }
        }
        Collections.reverse(locationNodes);
        System.out.println(title);
        System.out.println(String.join(" > ", locationNodes));
    }
}
